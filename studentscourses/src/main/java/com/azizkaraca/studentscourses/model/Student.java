package com.azizkaraca.studentscourses.model;

import java.io.Serializable;

import javax.persistence.*;

@Entity
public class Student implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false, updatable = false)
	private Long id;
	private String name;
	private String age;
	private String course;
	private String studentCode;
	
	public Student() {}
	
	public Student(String name, String age, String course, String studentCode) {
		this.name = name;
		this.age = age;
		this.course = course;
		this.studentCode = studentCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}
	
	public String getStudentCode() {
		return studentCode;
	}

	public void setStudentCode(String studentCode) {
		this.studentCode = studentCode;
	}
	
	@Override
	public String toString() {
		return "Student{" +
				"id=" + id + 
				", name='" + name + '\'' + 
				", age='" + age + '\'' + 
				", course='" + course + '\'' +
				'}';
				
	}

}
